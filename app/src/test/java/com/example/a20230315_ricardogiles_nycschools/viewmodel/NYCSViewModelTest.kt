package com.example.a20230315_ricardogiles_nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230315_ricardogiles_nycschools.data.repository.Repository
import com.example.a20230315_ricardogiles_nycschools.util.ResponseType
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class NYCSViewModelTest {

    private lateinit var viewModel: NYCSViewModel
    private val mockRepository = mockk<Repository>(relaxed = true)
    private val testDispatcher = UnconfinedTestDispatcher()
    //private val testScope = TestScope(testDispatcher)

    @get:Rule val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        viewModel = NYCSViewModel(mockRepository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        clearAllMocks()
    }

    @Test
    fun `get schools when repository returns an error returns an error state`() {
        every { mockRepository.getNYCS() } returns flowOf(
            ResponseType.ERROR("Error")
        )
        viewModel.getSchools()

        NYCSViewModel(mockRepository)

        viewModel.nycsResult.observeForever {
            if (it is ResponseType.ERROR) {
                assertEquals(it.e, "Error")
            }
        }
    }

    @Test
    fun `get schools when repository returns an success returns an success state`() {
        every { mockRepository.getNYCS() } returns flowOf(
            ResponseType.SUCCESS(listOf(mockk(), mockk()))
        )
        viewModel.getSchools()

        NYCSViewModel(mockRepository)

        viewModel.nycsResult.observeForever {
            if (it is ResponseType.SUCCESS) {
                assertEquals(2, it.response.size)
            }
        }
    }
}